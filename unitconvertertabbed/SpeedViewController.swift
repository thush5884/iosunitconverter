//
//  SpeedViewController.swift
//  unitconvertertabbed
//
//  Created by Mac on 8/9/18.
//  Copyright © 2018 Macm. All rights reserved.
//

import UIKit

fileprivate enum Owner {
    case metre
    case feet
    case kilometere
    case miles
}

class SpeedViewController: UIViewController {

    @IBOutlet weak var metreTxt: UITextField!
    @IBOutlet weak var feetTxt: UITextField!
    @IBOutlet weak var kilometereTxt: UITextField!
    @IBOutlet weak var milesTxt: UITextField!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reset()
    }
    
    func reset(){
        metreTxt.text = ""
        feetTxt.text = ""
        kilometereTxt.text = ""
        milesTxt.text = ""
    }
    
    func updateFromMetre(value: Double)
    {
        feetTxt.text = String(value * 196.85)
        kilometereTxt.text = String( value * 3.6 )
        milesTxt.text = String(value * 2.23694)
    }
    
    func updateFromFeet(value: Double)
    {
        metreTxt.text = String(0.00508 * value)
        kilometereTxt.text = String(0.018288 * value)
        milesTxt.text = String(0.0113636 * value)
    }
    
    func updateFromKiloMeteres(value: Double)
    {
        metreTxt.text = String(0.27778 * value)
        feetTxt.text = String(54.6806649 * value)
        milesTxt.text = String(0.621371 * value)
    }
    
    func updateFromMiles(value: Double)
    {
        metreTxt.text = String(0.44704 * value)
        feetTxt.text = String(88 * value)
        kilometereTxt.text = String(1.60934 * value)
    }
    
    fileprivate func recCalculate(owner: Owner){
        switch owner {
        case .feet:
            updateFromFeet(value: Double(feetTxt.text!)!)
        case .kilometere:
            updateFromKiloMeteres(value: Double(kilometereTxt.text!)!)
        case .metre:
            updateFromMetre(value: Double(metreTxt.text!)!)
        case .miles:
            updateFromMiles(value: Double(milesTxt.text!)!)
        }
    }
    
    @IBAction func metreChange(_ sender: Any) {
        if(metreTxt.text != nil && !metreTxt.text!.isEmpty)
        {
            recCalculate(owner: Owner.metre)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func feetChange(_ sender: Any) {
        if(feetTxt.text != nil && !feetTxt.text!.isEmpty)
        {
            recCalculate(owner: Owner.feet)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func kilometereChange(_ sender: Any) {
        if(kilometereTxt.text != nil && !kilometereTxt.text!.isEmpty)
        {
            recCalculate(owner: Owner.kilometere)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func milesChange(_ sender: Any) {
        if(milesTxt.text != nil && !milesTxt.text!.isEmpty)
        {
            recCalculate(owner: Owner.miles)
        }
        else{
            reset()
        }
        
    }
    
    
    
}
