//
//  FirstViewController.swift
//  unitconvertertabbed
//
//  Created by Mac on 8/9/18.
//  Copyright © 2018 Macm. All rights reserved.
//

import UIKit

fileprivate enum Owner {
    case gram
    case kilogram
    case pound
    case ounce
}

class FirstViewController: UIViewController {

    @IBOutlet weak var gramTxt: UITextField!
    @IBOutlet weak var kilogramTxt: UITextField!
    @IBOutlet weak var poundTxt: UITextField!
    @IBOutlet weak var ounceTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reset()
    }
    
    func reset(){
        gramTxt.text = ""
        kilogramTxt.text = ""
        poundTxt.text = ""
        ounceTxt.text = ""
    }
    
    fileprivate func recalculate(owner: Owner){
        switch owner {
            case .gram:
                updateFromGram(value: Double(gramTxt.text!)!)
            case .kilogram:
                updateFromKilogram(value: Double(kilogramTxt.text!)!)
            case .pound:
                updateFromPound(value: Double(poundTxt.text!)!)
            case .ounce:
                updateFromOunce(value: Double(ounceTxt.text!)!)
        
        }
    }
    
    func updateFromOunce(value: Double){
        gramTxt.text = String(value * 28.34952)
        kilogramTxt.text = String(value * 0.02834952)
        poundTxt.text = String(value / 16)
    }
    
    func updateFromPound(value: Double){
        gramTxt.text = String(value * 453.59237)
        kilogramTxt.text = String(value * 0.45359237)
        ounceTxt.text = String(value * 16)
    }
    
    func updateFromGram(value: Double)
    {
        kilogramTxt.text = String(value / 1000.0)
        poundTxt.text = String(value / 453.59237)
        ounceTxt.text = String(value / 28.34592)
    }
    
    func updateFromKilogram(value: Double){
        gramTxt.text = String(value * 1000)
        poundTxt.text = String(value / 0.45359237 )
        ounceTxt.text = String(value / 0.02834952)
    }

    @IBAction func gramChange(_ sender: Any) {
        if(gramTxt.text != nil && !gramTxt.text!.isEmpty)
        {
            recalculate(owner: Owner.gram)
        }
        else{
            reset()
        }
    }
    
   
    @IBAction func kiloGramChange(_ sender: Any) {
        if(kilogramTxt.text != nil && !kilogramTxt.text!.isEmpty)
        {
            recalculate(owner: Owner.kilogram)
        }
        else{
            reset()
        }
    }
    
    @IBAction func poundChange(_ sender: Any) {
        if(poundTxt.text != nil && !poundTxt.text!.isEmpty){
            recalculate(owner: Owner.pound)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func ounceChange(_ sender: Any) {
        if(ounceTxt.text != nil && !ounceTxt.text!.isEmpty){
            recalculate(owner: Owner.ounce)
        }
        else{
            reset()
        }
    }
    
    
    
    
}

