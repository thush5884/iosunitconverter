//
//  SecondViewController.swift
//  unitconvertertabbed
//
//  Created by Mac on 8/9/18.
//  Copyright © 2018 Macm. All rights reserved.
//

import UIKit

fileprivate enum Owner {
    case celsius
    case farenheit
    case kelvin
}

class SecondViewController: UIViewController {

    @IBOutlet weak var celsiusTxt: UITextField!
    @IBOutlet weak var faherinheitTxt: UITextField!
    @IBOutlet weak var kelvinTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        reset()
    }
    
    fileprivate func reCalculate(owner: Owner){
        switch owner{
            case .celsius:
                updateFromCelcius(value: Double(celsiusTxt.text!)!)
            case .farenheit:
                updateFromFarenheight(value: Double(faherinheitTxt.text!)!)
            case .kelvin:
                updateFromKelvin(value: Double(kelvinTxt.text!)!)
        }
    }
    
    func reset(){
        celsiusTxt.text = ""
        faherinheitTxt.text = ""
        kelvinTxt.text = ""
    }
    
    func updateFromKelvin(value: Double)
    {
        celsiusTxt.text = String(value - 273.15)
        faherinheitTxt.text = String((value * 9/5) - 459.67 )
    }
    
    func updateFromFarenheight(value: Double)
    {
        celsiusTxt.text = String((value - 32) / 1.8)
        kelvinTxt.text = String( (value + 459.67) * 5/9 )
    }
    func updateFromCelcius(value: Double){
        faherinheitTxt.text = String((value * 1.8) + 32)
        kelvinTxt.text = String(value + 273.15)
    }
    
    @IBAction func celciusChange(_ sender: Any) {
        if(celsiusTxt.text != nil && !celsiusTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.celsius)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func farenheightChange(_ sender: Any) {
        if(faherinheitTxt.text != nil && !faherinheitTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.farenheit)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func kelvinChange(_ sender: Any) {
        if(kelvinTxt.text != nil && !kelvinTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.kelvin)
        }
        else{
            reset()
        }
        
    }
    

}

