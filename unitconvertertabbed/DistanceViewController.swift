//
//  DistanceViewController.swift
//  unitconvertertabbed
//
//  Created by Mac on 8/9/18.
//  Copyright © 2018 Macm. All rights reserved.
//

import UIKit

fileprivate enum Owner {
    case metere
    case foot
    case yard
    case kilometere
    case mile
}

class DistanceViewController: UIViewController {

    @IBOutlet weak var metersTxt: UITextField!
    @IBOutlet weak var footTxt: UITextField!
    @IBOutlet weak var yardTxt: UITextField!
    @IBOutlet weak var kilometereTxt: UITextField!
    @IBOutlet weak var mileTxt: UITextField!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reset(){
        metersTxt.text = ""
        footTxt.text = ""
        yardTxt.text = ""
        kilometereTxt.text = ""
        mileTxt.text = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reset()
    }

    fileprivate func reCalculate(owner: Owner)
    {
        switch owner {
        case .foot:
            updateFromFoot(value: Double(footTxt.text!)!)
        case .kilometere:
            updateFromKiloMetere(value: Double(kilometereTxt.text!)!)
        case .metere:
            updateFromMetere(value: Double(metersTxt.text!)!)
        case .mile:
            updateFromMiles(value: Double(mileTxt.text!)!)
        case .yard:
            updateFromYard(value: Double(yardTxt.text!)!)
        }
    }
    
    func updateFromMetere(value: Double)
    {
        footTxt.text = String(value / 0.3048)
        yardTxt.text = String(value * 1.0936)
        kilometereTxt.text = String(value / 1000)
        mileTxt.text = String( value * 0.00062137)
    }
    
    func updateFromFoot(value: Double){
        metersTxt.text = String(value * 0.3048)
        yardTxt.text = String(value * 0.33333)
        kilometereTxt.text = String(value / 3280.8)
        mileTxt.text = String(value * 0.00018939)
    }
    
    func updateFromYard(value: Double){
        metersTxt.text = String(value / 1.0936)
        footTxt.text = String(value * 3.0000)
        kilometereTxt.text = String(value * 0.0009144)
        mileTxt.text = String(value * 0.00056818)
    }
    
    func updateFromKiloMetere(value: Double)
    {
        metersTxt.text = String(value * 1000)
        footTxt.text = String(value * 3280.8)
        yardTxt.text = String(value * 1093.61398)
        mileTxt.text = String(value / 1.609344)
    }
    
    func updateFromMiles(value: Double){
        metersTxt.text = String(value / 0.00062137)
        footTxt.text = String(value * 5280.0)
        yardTxt.text = String(value * 1760.0)
        kilometereTxt.text = String(value * 1.609344)
    }
    
    @IBAction func metereChange(_ sender: Any) {
        if(metersTxt.text != nil && !metersTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.metere)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func footChange(_ sender: Any) {
        if(footTxt.text != nil && !footTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.foot)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func yardChange(_ sender: Any) {
        if(yardTxt.text != nil && !yardTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.yard)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func kilometereChange(_ sender: Any) {
        if(kilometereTxt.text != nil && !kilometereTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.kilometere)
        }
        else{
            reset()
        }
        
    }
    
    @IBAction func mileChange(_ sender: Any) {
        if(mileTxt.text != nil && !mileTxt.text!.isEmpty)
        {
            reCalculate(owner: Owner.mile)
        }
        else{
            reset()
        }
        
    }
    
    
    
    
}
